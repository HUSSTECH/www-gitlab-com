---
layout: handbook-page-toc
title: "Distribution Team Demo"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Weekly Demo ##

Each Distribution team engineer is expected to present a demo related to an issue that was recently resolved, or a significant milestone in a long running epic.

### Goals ###
The goals of our demos are to:
1. Spread awareness of current issues impacting our customers and team
1. Solicit feedback from the team to improve our solutions and related documentation
1. Identify gaps in our collective understanding of environments, components, processes and dependencies
1. Improve visibility of the work our team does

### Presenter ###

The demo presenter typically rotates as part of the [triage](triage.html) responsibilities for a given week, but non-team members are welcome to present upon request. It is the presenter's responsibility to reschedule the demo if needed.

If unsure of what items to highlight in a demo, check for issues with the [Demo-worthy label](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Demo-worthy) If you are working on an issue that you would like to see demonstrated, feel free to add the label and/or announce in #g_distribution.

The presenter is expected to:

1. Prepare any required environmental and related dependencies ahead of the demo
1. Include a demo synopsis in the meeting notes with key component/environment versions, etc. for reproducibility
1. Limit distractions and keep professionalism high by muting slack and presenting in a demo conducive location
1. Follow up on concerns raised during the demo and/or provide links to known issues identified

### Attendees ###
Attendess are expected to:
1. Ask questions - attendee participatation is encouraged, but not required
1. Document questions/problems that arise in the meeting notes document
1. Mute yourself while not actively participating to avoid distractions
1. Be internal attendees from Distribution with other teams always welcome

Demo sessions are recorded and uploaded to YouTube GitLab Unfiltered [Distribution Demos](https://www.youtube.com/playlist?list=PL05JrBw4t0KrPasGZcEUoHHIYdUtzpfA4) and are publicly available.
