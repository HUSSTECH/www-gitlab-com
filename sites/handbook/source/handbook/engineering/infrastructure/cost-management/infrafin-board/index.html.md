---
layout: handbook-page-toc
title: "Infrafin"
description: "Infrafin Board"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----

## Criteria
While we would like each team to be as efficient as possible, it is not scalable or realistic for us to talk to each individual about one server or a few servers they are spinning up. For that reason we provide learning resources for those individuals to try and price out their own resources and then we define the following minimum requirements for something to be considered as part of the infrafin board. If the issue meets any of the following criteria and it is within our scope of control (part of central billing that we have visibility into) it can be added as a potential infrafin issue.
- Impact of issue (current or near future) is $10K/quarter or more
- Issue is part of an OKR for the current quarter
- If cost is less than $10K/quarter, but month over month cost has doubled for a particular team or service for an unknown or unplanned reason

## Prioritization

Weighting of infrafin issues is done in a similar fashion to RICE framework, but with the 4 factors below considered as part of the weighting. The weights are summed and then divided by 4 to normalize all the weights between 0 and 10, with 10 being the most important or impactful

### Weight Factors
#### Cost Savings
Measure of savings as a result of resolving the issue on a quarterly basis. This measure should always be calculated pre-discounts for weighting to maintain consistent weighting across GCP projects and across vendors.

| Factor Value    | Weight |
|-----------------|--------|
| < $10K/QTR      |    0.5 |
| $10K-$25K/QTR   |      2 |
| $25K-$50K/QTR   |      4 |
| $50K-$100K/QTR  |      6 |
| $100K-$200K/QTR |      8 |
| $200K-$500K/QTR |      9 |
| > $500K/QTR     |     10 |

#### Customer Impact
Factor meant to capture potential negative consequences of a change if production changes to the application are required. There are some cases where an infrafin issue would have a beneficial impact to customers also, but since this is only meant to capture negative effects, a large but beneficial customer impact should be weighted as low customer impact.

| Factor Value         | Weight |
|----------------------|--------|
| < 5% of Users        |     10 |
| 5-25% of Users       |      6 |
| 25%-50% of Users     |      3 |
| 50-80% of Users      |    1.5 |
| 80% or more of Users |    0.5 |

#### Future Potential Cost Impact
Measure of future cost that will be incurred if nothing is done to resolve an issue. This measure should always be calculated pre-discounts for weighting to maintain consistent weighting across GCP projects and across vendors. This measure is usually quite speculative, but it is meant to capture both growth in costs or in cases of abuse or un-needed servers the future cost of not addressing these problems.

| Factor Value    | Weight |
|-----------------|--------|
| < $10K/QTR      |    0.5 |
| $10K-$25K/QTR   |      2 |
| $25K-$50K/QTR   |      4 |
| $50K-$100K/QTR  |      6 |
| $100K-$200K/QTR |      8 |
| $200K-$500K/QTR |      9 |
| > $500K/QTR     |     10 |

#### Effort Required
This factor captures the general timeline for how much time and effort is required to resolve the issue.

| Factor Value | Weight |
|--------------|--------|
| < 1 week     |     10 |
| 1 wk - 1mo   |      7 |
| 1 mo - 3mo   |      5 |
| 3 mo - 6 mo  |      3 |
| 6 mo - 1 yr  |      2 |
| 1 yr - 2 yr  |      1 |
| > 2 yr       |      0 |


## Infrafin Board

### Short Term Saving Initiatives Process
Some savings initiatives can happen on a much horter time horizon, either because of the urgency or because we can handle the implmenetation from within the infrastructure department. Some examples of this include purchasing CUD from GCP, or AWS savings plan to optimize the cost of our servers across the whole company, or investigating and fixing specific billing anomalies that we see over time.

#### 1. Initial Data Exploration
In this scenario, the issue is already known,  potentially because of earlier data exploration or other work, but there is still usually some data exploration to do to understand these issues better.

#### 2. Initial Cost Impact Analysis
Quantify what the impact of the issue at hand is

#### 3. Identify and Partner with SME

Partner with whoever the SME for the area is to find out how the issue can be resolved.

#### 4. Resolve the issue

Due to the nature of being bucketed as a short term initiative, these should be relatively low effort, high impact initiatives and generally these should be completed as soon as possible. The weights on these issue will automatically be set to 8

#### 5. Follow-up
After the change has been made, confirm the numbers match close to what was expected.



### Longer Term Saving Initiatives Process
These are initiatives that require cross-team collaboration and major changes in a service or infrastructure to happen. Some examples include implementing online registry garbage collection, optimizing our internal CI usage, and changing machine types or storage types for one of our major services.

#### 1. Initial Data Exploration

The first step is to take a macro view of our infrastructure to understand where we are spending most of our money today, how fast is it growing, and where are we likely to spend more money in the future
This can be done using whatever tools are available, but today this is mainly done by going into the billing consoles for the various vendors to get a better understanding of where our spend is going.

#### 2. Decide on focus area

There are many different factors to choose from when it comes to deciding on which issues to tackle, but the main factors in descending order of important would be:

- Cost
- Customer impact
- Current and future potential growth
- Ease of solution implementation

There are some ways we can condense this data down to make it easier, such as by using a weighted MoM growth metric around certain dimensions to see what areas of our spend are both very high and also continuing to grow at a rapid pace.

#### 3. Initial Cost Estimate

Once an area to focus on has been chosen, then we can come up with a proposal about how we could change or implement something to optimize our cost, and come up with a cost estimate of what this might be. This will be the quarterly savings label, and this is the main step as an IA to do, which will help give finance and PM's a good understanding of how they want to prioritize the issue.

#### 4. Estimate of effort and timeline by SME

SME for the service/area being analyzed will decide how difficult and time intensive this solution would be to implement

#### 5. Initial Risk Estimate

Based on what SME has to say and the cost estimate, and projected growth in future, a low/medium/high risk label is assigned which specifies how much more of a problem this could be in the future if nothing is done soon, and the potential to materially impact our budget plan

#### 6. Prioritization Decision Made

PM and finance partner decide where this should fall in roadmap.

If risk is high and cost savings estimated to be over $50k/quarter, then exec sponsor chosen to oversee project to completion.

#### 7. Follow-up
Analyze the real impact of the change and expectations going forward.
