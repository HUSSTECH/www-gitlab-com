---
layout: handbook-page-toc
title: "Infrastructure Analyst Role"
description: "Defines role and responsibilites of infra analyst"
---


## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----

### Responsibilities
- Optimize infra spend through committed spend programs and supporting enterprise contract deals
- Ad Hoc Analysis of Infrastructure hosting spend
- Weekly Status meeting of cost savings initiatives tracked by infrafin board
- Dashboard and chart creation to improve observability into infrastructure costs
- Teaching and partnering with PM's and others on how to understand the cost and usage of their services as it relates to our vendors

### Working with an infrastructure analyst

There is no magic when reducing or analyzing infrastructure spend, it just requires a good understanding of what our own usage and architecture looks like and how we get billed for it.

Therefore, if requesting help or a review from an infrastructure analyst you should try to provide as much information as possible. Some of the most important pieces of information to provide are:
- description or link to architecture of affected services
- data Source of usage info and what metrics best relate to the cost if known
- Affected product and/or sku of service provider

If one or more of these is not known, then you should partner with the infrastructure analyst so you can define these together.
