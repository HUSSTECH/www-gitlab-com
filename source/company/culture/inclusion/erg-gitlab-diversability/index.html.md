---
layout: markdown_page
title: "ERG - GitLab DiversABILITY"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction
We will start planning a launch event for the DiversABILITY TMRG in July 2020.  We look forward to providing helpful resources, support, and activities for our team members who are differently abled or who are caring for a differently abled loved one.


## Mission 

To provide everyone a safe, inclusive, respectful environment where they can use their unique gifts to contribute, lead, and prosper.


## Leads
* [Melody Maradiaga](https://about.gitlab.com/company/team/#mmaradiaga) 
* [Keith Snape](https://about.gitlab.com/company/team/#Keith%20Snape)




## Upcoming Events




## Additional Resources
