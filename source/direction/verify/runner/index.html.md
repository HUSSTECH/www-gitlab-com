---
layout: markdown_page
title: "Category Direction - Runner"
description: "GitLab Runner is the multi-platform execution agent that works with GitLab CI to execute the jobs in your pipelines. View more information here!"
canonical_path: "/direction/verify/runner/"
---

- TOC
{:toc}

## GitLab Runner

GitLab Runner is the multi-platform execution agent that works with [GitLab CI](/direction/verify/continuous_integration)
to execute the jobs in your pipelines. Our vision is for Runners on GitLab.com to be the most full-featured, high-performant CI build fleet in the cloud.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARunner)
- [Overall Vision](/direction/ops#verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

## Organizational Adoption Journey

```mermaid
graph LR
   classDef blue fill:#3278ca, color:#fff  
   classDef darkblue fill:#225798, color:#fff

   subgraph "CUSTOMER JOURNEY"
      A0[User Actions]:::darkblue
   end
   subgraph "POC, ONBOARDING, PRODUCTION ENVIRONMENT SETUP"
      A1[Install GitLab]:::blue --> A2(Install the 1st Runner):::blue
   end
   subgraph "SCALE USAGE PHASE: USERS, FEATURES, STAGE ADOPTION, n...n+1 phases"
      A2 --> A3(Rollout and start managing more than 1 Runner to support the organization):::blue
   end
   A0-.-A1;
```

```mermaid
graph LR
   classDef red fill:#c92d39, color:#fff
   classDef darkred fill:#9a0814, color:#fff
   classDef blue fill:#3278ca, color:#fff  
   classDef darkblue fill:#225798, color:#fff

   subgraph "CUSTOMER JOURNEY"
      A0[Runner Pain Points]:::darkred
   end
   subgraph "POC, ONBOARDING, PRODUCTION ENVIRONMENT SETUP"
      A1[Manual steps to install<br /> and configure first Runner]:::red --> A2(Manual steps to configure additional Runners):::red
   end
   subgraph "SCALE USAGE PHASE: USERS, FEATURES, STAGE ADOPTION, n...n+1 phases"
      A2 --> A3[Runner Token registration<br /> process is manual]:::red
      A3 --> A4[High operational overhead<br /> to maintain a large fleet of Runners]:::red
      A4 --> A5[No instrumentation that<br /> indicates why a Runner<br /> has not started]:::red
   end
   A0-.-A1;
```
## What's Next & Why

### Multi-Platform Support

- [Linux on zOS Docker Image](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/25320) We are working on releasing a GitLab Runner Docker image for IBM's Linux on Z to provide enhanced CI capabilities for the z/OS community.
- [macOS Runners on GitLab.com (Closed Beta)](https://gitlab.com/gitlab-org/gitlab/-/issues/224124) We have heard from several users and community members that need the ability to build macOS CI jobs on GitLab.com without having to set up and maintain self-managed macOS Runners. To address this need, we are working towards a closed beta launch of macOS Runners on GitLab.com.

### Make CI Easy for Self-Managed 

- [Make CI easy for Self-Managed GitLab users:](https://gitlab.com/groups/gitlab-org/-/epics/2778) Our goal is that users self-managed GitLab should be able to get up and running with CI with as little friction as possible.

## Maturity Plan

Since this category is already at the "Lovable" maturity level (see our
[definitions of maturity levels](/direction/maturity/)),
we do not have an upcoming target for bringing it to the next level. However, it's important to us that
we defend our lovable status and provide the most value to our user community.
To that end, we are starting to work on collecting community feedback to understand users' pain points with installing, managing, and using GitLab Runners. 
If you have any feedback that you would like to share, you can do so in this [epic](https://gitlab.com/gitlab-org/gitlab/issues/39281). 

Maturing the Windows executor and autoscaler is of particular importance as we improve our support for Windows. There are a few issues we've identified as being part of that effort:

- [Create VMs in background to speed-up the autoscaling](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/issues/32)
- [Distribute Docker windows image for gitlab-runner](https://gitlab.com/gitlab-org/gitlab-runner/issues/3914)
- [Support named pipes for Windows Docker executor](https://gitlab.com/gitlab-org/gitlab-runner/issues/4295)
- [Use Docker Save to publish docker helper images](https://gitlab.com/gitlab-org/gitlab-runner/issues/3979)
- [Add support for Windows device for Docker executor](https://gitlab.com/gitlab-org/gitlab-runner/issues/3923)

## Competitive Landscape

The Runner is currently evaluated as part of the comprehensive competitive analysis in the [Continuous Integration category](/direction/verify/continuous_integration/#competitive-landscape)

## Top Customer Success/Sales Issue(s)

For the CS team, the issue [gitlab-runner#3121](https://gitlab.com/gitlab-org/gitlab-runner/issues/3121) where orphaned
processes can cause issues with the runner in certain cases has been highlighted as generating
support issues.

## Top Customer Issue(s)

The top requested customer issue that we are investigating is [gitlab-runner#1809](https://gitlab.com/gitlab-org/gitlab-runner/issues/1809). 
The principal goal of this feature is to enable you to use variables declared in the .gitlab.yml file within other variables in that file.

Other popular issues include:

- [gitlab-runner#1057](https://gitlab.com/gitlab-org/gitlab-runner/issues/1057): Specify root folders for artifacts
- [gitlab-runner#6400](https://gitlab.com/gitlab-org/gitlab-runner/issues/6400): Make environment variables set in before_script available for expanding in .gitlab-ci.yml
- [gitlab-runner#1107](https://gitlab.com/gitlab-org/gitlab-runner/issues/1107): Docker Artifact caching
- [gitlab-runner#3392](https://gitlab.com/gitlab-org/gitlab-runner/issues/3392): Multi-line command output can be un-collapsed in Job terminal output view
- [gitlab-runner#3207](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3207): Configure docker volumes in .gitlab-ci.yml

## Top Internal Customer Issue(s)

- [gitlab#16319](https://gitlab.com/gitlab-org/gitlab/-/issues/16319): Allow registration of runners via API
- [gitlab#24674](https://gitlab.com/gitlab-org/gitlab/-/issues/24674): Backstage work to enable interactive web terminals on shared runners
- [gitlab#25969](https://gitlab.com/gitlab-org/gitlab/issues/25969): Allow advanced configuration of GitLab runner when installing GitLab managed apps
- [gitlab#26039](https://gitlab.com/gitlab-org/gitlab/-/issues/26039) Enable interactive web terminal for GitLab.com

## Top Vision Item(s)

### Migrating from Docker Machine for Runner Autoscaling

Autoscaling of GitLab Runner on Virtual Machines hosted on the major cloud platforms is done today with Docker Machine which is in [maintenance mode](https://forum.gitlab.com/t/docker-machine-is-now-in-maintenance-mode/29381). As such, a key strategic initiative is migrating away from Docker Machine for autoscaling. To follow along, or add feedback to this critical topic, please review the epic [gitlab-org&2502](https://gitlab.com/groups/gitlab-org/-/epics/2502).

### Shared runner billing and management for self-managed

We are also exploring enabling users of self-managed GitLab instances (CE and EE) to purchase CI minutes, [gitlab-org&835](https://gitlab.com/groups/gitlab-org/-/epics/835). This solution will also include management and reporting capability so that users can see a clear history of Runner minutes granted and consumed.

### Offer Premium Machine Types in Shared Runner Fleet

We currently offer a single size for runners in our shared fleet, but some jobs take more or less CPUs, and some take more or less RAM. Offering additional options will help users who are not comfortable with or not interested in running their own runners.

To follow along, or add feedback to this critical topic, refer to [Epic #2426](https://gitlab.com/groups/gitlab-org/-/epics/2426)

### Additional Platforms

A common request we get is to add support for various platforms to the Runner. We've chosen to support
the plethora of different systems out there by adding support for a [custom executor](https://docs.gitlab.com/runner/executors/custom.html) that can be overridden to support different needs. In this way platforms like [Lambda](https://gitlab.com/gitlab-org/gitlab-runner/issues/3128), [vSphere](https://gitlab.com/gitlab-org/gitlab-runner/issues/2122), and
even custom in-house implementations can be implemented.

There is high community interest from customers who have IBM z/OS Mainframe's and want to use GitLab Runners on that platform. To follow along, or add feedback to this critical topic, please review the epic [Epic #3144](https://gitlab.com/groups/gitlab-org/-/epics/3144).

Additional platforms are primarily being implemented through community contributions, so if you're interested in contributing to GitLab these issues are great ones to get involved with.
